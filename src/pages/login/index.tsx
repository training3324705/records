import { Link, useNavigate } from 'react-router-dom'
import Auth from '../../layouts/auth'
import Google from '../../assets/googleimg.png'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useState } from 'react'
interface FormInputs {
  email: string
  password: string
}

const schema = yup.object().shape({
  email: yup.string().email().required('Email is a required field'),
  password: yup.string().required('Please enter your password'),
  // .matches(
  //   /^.*(?=.{4,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
  //   // "Password must contain at least 8 characters, one uppercase, one number and one special case character"
  //   'Password must contain at least 4 characters',
  // ),
})

const Login = () => {
  const [errror, setError] = useState('')
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormInputs>({
    resolver: yupResolver(schema),
  })

  const navigate = useNavigate()

  const onSubmit = (data: any) => {
    // console.log(data, 'dataa')
    axios.post('https://stagging-med-server.onrender.com/hosp/login', data).then((response) => {
      // console.log(response.data, 'api res')
      localStorage.setItem('uerLogedIn', response.data.success)
      const userLogedIn = localStorage.getItem('uerLogedIn')
      userLogedIn === 'true' ? navigate('/dashboard') : navigate('/login')
      userLogedIn === 'false' ? setError('Invalid Credentials') : null
    })
  }

  return (
    <Auth>
      <div className='container mx-auto px-4 h-full'>
        <div className='flex content-center items-center justify-center h-full'>
          <div className='w-full lg:w-4/12 px-4'>
            <div className='relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0'>
              <div className='flex content-center items-center justify-center h-full pt-5 pb-5 text-xl font-black text-gray-900 '>
                Sign in
              </div>
              <div className='flex-auto px-4 lg:px-10 py-10 pt-0'>
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className='relative w-full mb-3'>
                    <label
                      className='block uppercase text-blueGray-600 text-xs font-bold mb-2'
                      htmlFor='grid-password'
                    >
                      Email
                    </label>
                    <input
                      className='border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150'
                      placeholder='Email'
                      type='Email'
                      {...register('email')}
                    />
                    {errors.email && <div>{errors.email?.message}</div>}
                  </div>

                  <div className='relative w-full mb-3'>
                    <label
                      className='block uppercase text-blueGray-600 text-xs font-bold mb-2'
                      htmlFor='grid-password'
                    >
                      Password
                    </label>
                    <input
                      className='border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150'
                      placeholder='Password'
                      type='Password'
                      {...register('password')}
                    />
                    {errors.email && <div>{errors.password?.message}</div>}
                    {errror && <h1 style={{ color: 'red' }}>{errror}</h1>}
                  </div>

                  <div className='text-center mt-6'>
                    <button
                      className='bg-blueGray-800 text-black active:bg-blueGray-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150'
                      type='submit'
                    >
                      Sign In
                    </button>
                  </div>
                </form>
                <hr className='mt-6 border-b-1 border-blueGray-300' />
                <div className='rounded-t mb-0 px-6 py-6'>
                  <div className='text-center mb-3'>
                    {/* <h6 className='text-blueGray-500 text-sm font-bold'>Or</h6> */}
                  </div>
                  <div className='btn-wrapper text-center'>
                    <button
                      className='bg-white active:bg-blueGray-50 text-blueGray-900 px-4 py-2 rounded outline-none focus:outline-none mr-1 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs ease-linear transition-all duration-150'
                      type='button'
                    >
                      Sign in with Google
                      <img alt='...' className='w-5 mr-1' src={Google} />
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className='flex flex-wrap mt-6 relative'>
              <div className='w-1/2'>
                <a href='#pablo' onClick={(e) => e.preventDefault()} className='text-blueGray-200'>
                  <small>Forgot password?</small>
                </a>
              </div>
              <div className='w-1/2 text-right'>
                <Link to='/register'>
                  <a href='#pablo' className='text-blueGray-200'>
                    <small>Create new account</small>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Auth>
  )
}

export default Login
