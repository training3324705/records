// components

import CardTable from '../../components/Cards/CardTable'

// layout for page

import Admin from '../../layouts/admin'

export default function Appointment() {
  return (
    <Admin>
      <div className='flex flex-wrap mt-4'>
        <div className='w-full mb-12 px-4'>
          <CardTable />
        </div>
        {/* <div className='w-full mb-12 px-4'>
          <CardTable color='dark' />
        </div> */}
      </div>
    </Admin>
  )
}
