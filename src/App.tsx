import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './App.css'
import Appointment from './pages/appointment'
import DashBoard from './pages/dashboard'
import Landing from './pages/landing'
import Login from './pages/login'
import Profile from './pages/profile'
import Register from './pages/register'
import Settings from './pages/settings'

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Landing />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
          <Route path='/dashboard' element={<DashBoard />} />
          <Route path='/appointment' element={<Appointment />} />
          <Route path='/profile' element={<Profile />} />
          <Route path='/settings' element={<Settings />} />
        </Routes>
      </BrowserRouter>
    </>
  )
}

export default App
