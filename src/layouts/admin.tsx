import AdminNavbar from '../components/NavBars/AdminNavbar'
import Sidebar from '../components/SideBar/index'
import HeaderStats from '../components/Headers/HeaderStats'

export default function Admin({ children }: any) {
  return (
    <div className='relative md:ml-64 bg-blueGray-100'>
      <Sidebar />
      <AdminNavbar />
      <HeaderStats />
      <div className='px-4 md:px-10 mx-auto w-full -m-24 mt-10'>{children}</div>
    </div>
  )
}
